# TextFacter

This is a small project made as my introductionary step to the world of Python.
A rather simple webapplication build on the Flask micro-framework, also implementing
the Python-intregrated SQLite3 RDBMS. This was done following the official Flask
documentation, but I added something of "my own". 
The app is contains a login and a blogpost system, the latter implementing the
TextBlob API, so that the user may get an analysis of the blogpost.
The analysis will show the user the sentimental values and the objective/subjective
polarity.

## Setup guide

Download Python from the offical website, and you should be good to go.
For thorough documentation ontop of comments go to:
flask.palletsprojects.com/
&
https://textblob.readthedocs.io/en/dev/


# License
---

MIT

Copyright 2021 Kristoffer Alexander Fich

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.