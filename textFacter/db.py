import sqlite3

import click
from flask import current_app, g
from flask.cli import with_appcontext

 # Here we make a function creating and returning the database.
def get_db():

 # If the database connection hasn't been created, the imported request-specific object 'g' is assigned a connection
 # and two parameters are taken in: DATABASE-key from our application configuration and parsing out databases column types.
    if 'db' not in g:
        g.db = sqlite3.connect(
            current_app.config['DATABASE'],
            detect_types=sqlite3.PARSE_DECLTYPES
        )
 # Lastly this code is for making the database columns accesible by their names/titles. And then the function
 # will return the database connection object.        
        g.db.row_factory = sqlite3.Row

    return g.db

 # Here we make a function for closing the database connection. 
def close_db(e=None):

 # First we call the pop function to remove the attribute from the db variable.    
    db = g.pop('db', None)

 # And then database connection is closed if it exists.
    if db is not None:
        db.close()

 # This function is for initializing a database file relative to the root folder(TextFacter in this case).
def init_db():

 # First we assign the database connection and all its data to a local variable.    
    db = get_db()

 # Then we make the aforementioned folder.
    with current_app.open_resource('schema.sql') as f:
        db.executescript(f.read().decode('utf-8'))

 # Here a custom CLI command is defined, so that the database may be initialized from the command prompt.
 # This also clears any existing entries in the database.
@click.command('init-db')
@with_appcontext
def init_db_command():
    init_db()
    click.echo('Initialized the database')

# Here we define a method for registering the 'close_db' function and the custom CLI command with the application.
# A function is made as the application object only exists in the Application Factory function.
def init_app(app):

    # First we call a Flask function taking the 'close_db' as parameter. Making it so that 'close_db'
    # is also called when request context is popped.
    # Lastly the custom CLI command is registered with the application.
    app.teardown_appcontext(close_db)
    app.cli.add_command(init_db_command)
