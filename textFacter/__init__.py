import os
from flask import Flask

#Following code is an Application Factory. Meaning we instantiate the Flask application object within
# a function. This is mainly done for testing purposes.
def create_app(test_config=None):

 # We feed the application objects constructor two arguments. First being the name of the Python module
 # and then the applications configuration is set to be instance-relative.
    app = Flask(__name__, instance_relative_config=True)

 # Then a default configuration is set: SECRET_KEY is set to 'dev', but at deployment is set to a random
 # value. DATABASE is for storing the path to the Sqlite database.
    app.config.from_mapping(
        SECRET_KEY = 'dev',
        DATABASE=os.path.join(app.instance_path, 'textFacter.sqlite'),
    )

 # If no test-configuration file is fed through the Application Factory's constructor, then a default
 # configuration file is loaded from the instance folder, and silent failure is set so missing files won't
 # trigger an error. The config.py file is where deployment configurations will be written.

    if test_config is None:
        app.config.from_pyfile('config.py', silent=True)
    else:
        app.config.from_mapping(test_config)

 # After the configuration is done, the instance folder path is setup.
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass   
 
 # This code imports the database object and calls its 'init_app' function, thereby registering the needed
 # functions with the Flask application object, that was fed as a parameter.
    from . import db
    db.init_app(app)
    
 # Import and register the blueprint(s).   
    from . import auth
    app.register_blueprint(auth.bp)

    from . import blog
    app.register_blueprint(blog.bp)
    app.add_url_rule('/', endpoint='index')
    return app

    





