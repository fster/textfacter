import functools

from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)
from werkzeug.security import check_password_hash, generate_password_hash

from textFacter.db import get_db

# This code creates a blueprint. The Blueprint constructor takes three parameters:
# First the title/name of the blueprint, then the name of the blueprints package for locating root path
# for the blueprint, and lastly a url prefix is given matching the title of the blueprint.
bp = Blueprint('auth', __name__, url_prefix='/auth')

# Here a route is created with two parameters. First being the URL name associated with the route, and then
# the http methods used by the code associated with the route.
@bp.route('/register', methods=('GET', 'POST'))

 # Here the definition of the function associated with the register view is made.
def register():

 # If the request method called by the user input is POST, then the form data from the 'request.form' dict
 # is assigned to the appropiate local variable. An instance of connection to database is created. And a None-valued error variable is made for later use.
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        db = get_db()
        error = None
     
     # If no user input is present in either variable, then the error variable is assigned an appropiate message.
     # Should none of the first to branches trigger, then the code runs a check on the data attempted registered
     # for a username against the database entries. And if an entry matches 'fetchone' will be 'not None',
     # thereby defining the error message appropiately.
        if not username:
            error = 'Username is required'
        elif not password:
            error = 'Password is required'
        elif db.execute(
            'SELECT id FROM user WHERE username = ?', (username,)
        ).fetchone() is not None:
          error = 'User {} is already registered. '.format(username)

        # This branch triggers if no error-branches were triggered in the code above.
        # The db's 'execute' function is called to define the SQL statement for adding the form data with
        # a hashed password. Then 'commit' is called as this query adds data, so the changes needs to be saved.
        # After the user has been succesfully registered, they are redirected to the login route.
        if error is None:
            db.execute(
                'INSERT INTO user (username, password) VALUES (?, ?)',
                (username, generate_password_hash(password))
            )
            db.commit()
            return redirect(url_for('auth.login'))
        # Should a value be present in 'error' variable the 'flash' function is called. The function
        # stores messages that can be rendered to the html template.
        flash(error)
    return render_template('auth/register.html')    

                


# This view route and its structure works much the same as the one above does. With a few exceptions though.
@bp.route('login', methods=('GET', 'POST'))
def login():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        db = get_db()
        error = None
        user = db.execute(
            'SELECT * FROM user WHERE username = ?', (username,)
        ).fetchone()

        # 'check_password_hash' compares the hashed password entered by the user to the hashed password
        # actually associated with user entry pulled from the database, if it matched the username recently entered by the user.
        if user is None:
            error = 'Incorrect username.'
        elif not check_password_hash(user['password'], password):
            error = 'Incorrect password.'
        
        # If error has no value this branch triggers. 
        # The session variable is a dictionary where requests and their associated data are stored.
        # The current session is cleared, and a new session is created containing the succesfully logged in
        # user's id. Making it available for following requests. Then the code redirects to another URL.
        if error is None:
            session.clear()
            session['user_id'] = user['id']
            return redirect(url_for('index'))
        flash(error)
    return render_template('auth/login.html')


# This code runs and registers a function, before any URL is requested. It checks if the current session
# contains a 'user_id'.
@bp.before_app_request
def load_logged_in_user():
    user_id = session.get('user_id')
    
    # If no value is detected in session. The global objects user attribute is set to none.
    # Should the current session contain a value, then all data matching the value is selected from the
    # database and saved in the g object.
    if user_id is None:
        g.user = None
    else:
        g.user = get_db().execute(
            'SELECT * FROM user WHERE id = ?', (user_id,)
        ).fetchone()

# This code handles the user log out. 
# To log out the session needs to be cleared and the redirect...
@bp.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('index'))

# The following code makes accessing any views in the application require being logged in. 
def login_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.user is None:
            return redirect(url_for('auth.login'))
        return view(**kwargs)
    return wrapped_view
